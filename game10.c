/** @file  game10.c
    @author Jamie Sherriff and Josh Shaw with support from Michael Hayes   
    @date   2013
    @brief  Rock paper scissors game for the UCFK4

*/


#include "navswitch.h"
#include "led.h"
#include "../fonts/font5x7_1.h"
#include <stdlib.h>
#include "ir_uart.h"
#include "task.h"
#include "uint8toa.h"
#include "button.h"
#include "gamefunction.h"
#include <string.h>

/* Interactive two person game that is played by two people, each with
 * a UCFK4 board. Players simultaneously form one of three shapes,
 * consistening of a rock,paper and sisccors. The game plays in the following:
 * The "rock" beats scissors, the "scissors" beat paper and the "paper" beats 
 * rock and if both players choose the same a draw is called.
 *  
*/


#define DISPLAY_TASK_RATE 300
#define IR_UART_TX_TASK_RATE 40
#define IR_UART_RX_TASK_RATE 40
#define NAVSWITCH_TASK_RATE 40
#define BUTTON_TASK_RATE 40
#define TIMEOUTLIMIT 60
#define INDEXLIMIT 3
#define WINLIMIT 2
#define PLAYER  1
#define OPPONENT  0
#define PWM_RATE 40

/* Define text update rate (characters per 10 s).  */
#define MESSAGE_RATE 25


/* Master variables refer to the board itself and assumes the board it communicates 
 * with is the opponent*/
static uint8_t index, master_win_count, op_win_count, timeout, led_count = 0;
static uint8_t master_Choice, opponent_choice, choice = 'X';
static uint8_t pwm_tick = 0;
static uint8_t led_stuff = 0;


static Bool move_chosen, round_complete, master_won, slave_won = false;
static Bool reset, game_complete, sent_move = false;
static Bool winner_found, valid_screen, waiting, display_score = false;
            




/** Initilizes the display by setting up TinyGL  with a rate and scroll mode*/
static void display_task_init (void){
    tinygl_init (DISPLAY_TASK_RATE);
    tinygl_font_set (&font5x7_1);
    tinygl_text_speed_set (MESSAGE_RATE);
    tinygl_text_mode_set (TINYGL_TEXT_MODE_SCROLL);
    tinygl_text (GAMENAME);   
}


/** Initilizes the LED */
static void led_flash_task_init (void){
    led_init ();
}


/** Toggle the state of the LED */
static void led_flash_task (void){
    static uint8_t state = 1;
    led_set (LED1, state);
    state = !state;      
}


/** Resets the game and starts the game again while keeping the win counter */
void reset_game(void){
    system_init ();
    ir_uart_init ();
    display_task_init();

    reset=true;
    move_chosen = valid_screen = round_complete = master_won = false;
    display_score = slave_won = sent_move = winner_found = false;
    timeout = 0;
    master_Choice = opponent_choice = choice = 'X';
    led_set (LED1, 0);
    game_complete = false;
}


 /**Read input from  Button1 and update */   
static void button_task (__unused__ void *data){
    button_update ();
    
    if (button_push_event_p (BUTTON1)){
        reset_game();
    }
}


/**Resests the player win count to 0*/   
void reset_win_count(uint8_t player){
    if (player == 0){
        master_won = false;
        slave_won = true;
    }
    else{
        slave_won = false;
        master_won = true;
    }
    winner_found = true;
    game_complete = true;
    master_win_count = op_win_count= 0;
}


/** Takes the parameter of which player won overall and displays the
 * appropriate  message */
void who_won(uint8_t player){
    tinygl_text_mode_set (TINYGL_TEXT_MODE_SCROLL);
    if (player == 0){
        tinygl_text(FINAL_LOOSE);
    }
    else{
        tinygl_text(FINAL_WIN);
    }
    tinygl_update ();
    game_complete = false;
}


 /**Display information on the LED matrix and update it
  * If moves have been move_chosen the display will update the winner*/   
static void display_task (__unused__ void *data){
    char* text;
    tinygl_update ();
    
    if(op_win_count >= WINLIMIT && !game_complete){
        reset_win_count(0);
    }
    if(master_win_count >= WINLIMIT && !game_complete){
        reset_win_count(1);
    }
    if(!reset && !winner_found){
        if(round_complete && sent_move && !display_score && !game_complete){
            tinygl_text_mode_set (TINYGL_TEXT_MODE_SCROLL);
            find_winner(&master_Choice, &opponent_choice, &master_won,&slave_won);
            text = display_winner(&master_won,&slave_won, &master_win_count,&op_win_count);
            tinygl_text(text);
            tinygl_update ();
            winner_found = true;
        }
    }
    if(reset && (!round_complete || !sent_move)){
        reset = false;
    }
    if(sent_move && !round_complete && !waiting){
        tinygl_text_mode_set (TINYGL_TEXT_MODE_SCROLL);
        tinygl_text(WAITMESSAGE);
        waiting = true;
    }
    if(game_complete){ 
        if(master_won){
            who_won(1);
        }
        if(slave_won){
            who_won(0);
        }
    }       
}


/**Displays the the int master_win_count as a string for TinyGL
 * If the wincount is greater than WINLIMT the count is reset */
void display_count(uint8_t number, uint8_t player){
    char buffer[4];
    if (number >= WINLIMIT){
        number = 0;
    }
    uint8toa (number, buffer, 0);
    tinygl_text_mode_set (TINYGL_TEXT_MODE_SCROLL);
    if (player == 0){
        char text[] = OPPONENT_SCORE;
        text[strlen(text)-1] = buffer[0];
        tinygl_text (text);
    }
    
    else{
        char text[] = PLAYER_SCORE;
        text[strlen(text)-1] = buffer[0];
        tinygl_text (text);
    }
    winner_found = false;
}


/** Push event for west button, sets waiting,display score to false and
 * if move has not been chosen shows the next display */
void navswitch_west(void){
    waiting = false;
    display_score = false;
    if (!move_chosen){
        tinygl_text_mode_set (TINYGL_TEXT_MODE_STEP);
        show_char(&index, &choice);
        index++;
        valid_screen = true;
    }
}


/** Push event for east button, sets waiting,display score to false and
 * if move has not been chosen shows the next display */
void navswitch_east(void){
    waiting = false;
    display_score = false;
    if(!move_chosen){
        tinygl_text_mode_set (TINYGL_TEXT_MODE_STEP);
        show_char(&index, &choice);
        index++;
        valid_screen = true;
    }
}


/** Push event for south button, displays the opponents score */
void navswitch_south(void){
    display_count(op_win_count,OPPONENT);
    valid_screen = false;
    display_score = true;
}


/** Push event for south button, displays your score */
void navswitch_north(void){
    display_count(master_win_count,PLAYER);
    valid_screen = false;
    display_score = true;
}

/** Push event for push button, sets the move and sets move_chose to true */
void navswitch_push(void){
    display_score = false;
    if(!move_chosen&& valid_screen){
        master_Choice = choice;
        move_chosen = true;
    }
}


/**Reads input from the Navswitch and calls the approiate function */
static void navswitch_task (__unused__ void *data){
    
    if (index >= INDEXLIMIT){
    index = 0;
    }
    navswitch_update ();
    
    if (navswitch_push_event_p (NAVSWITCH_WEST)){
        navswitch_west();
    }
    
    if (navswitch_push_event_p (NAVSWITCH_EAST)){
        navswitch_east();
    }
    
    if (navswitch_push_event_p (NAVSWITCH_SOUTH)){
        navswitch_south();
    }
    
    if (navswitch_push_event_p (NAVSWITCH_NORTH)){
        navswitch_north();
    }
    
    if (navswitch_push_event_p (NAVSWITCH_PUSH)){
        navswitch_push();
    }
}


/** Checks the recieved data from UART and ignores if not valid*/
void check_recieved_data(uint8_t data){
    if (data == ROCK){
        led_flash_task();
        round_complete = true;
        opponent_choice = ROCK;
    }
    else if (data == PAPER){
        led_flash_task();
        round_complete = true;
        opponent_choice = PAPER;
    }   
    else if (data == SCISSORS){
        led_flash_task();
        round_complete = true;
        opponent_choice = SCISSORS;
    }
}


/** Read character from USART1. Checks to see if it is valid data and store it*/
static void ir_uart_rx_task (__unused__ void *data){
        if (ir_uart_read_ready_p ()){
            uint8_t data;
            data = ir_uart_getc ();
            check_recieved_data(data);
        }
}


/** Write master_Choice to USART as long as TIMEOUTLIMIT has not been exceeded*/
static void ir_uart_tx_task (__unused__ void *data){  
    if (timeout < TIMEOUTLIMIT){ 
        led_stuff++;
        if (move_chosen && valid_screen){
           ir_uart_putc (master_Choice);
           timeout++;
           sent_move = true;
           led_flash_task();
        }  
    }

}

 static void led1_task (__unused__ void *data)
 {
    led_count++;
         if (led_count%50==0){
           led_set (LED1, 0);
             }
         
         else{
             led_set (LED1, 0);
         }  
 }


/** Main function that utilizes the task scheduler and intilzies all the
 * modules*/
int main (void){
    task_t tasks[] ={
        {.func = display_task, .period = TASK_RATE / DISPLAY_TASK_RATE},
        {.func = ir_uart_rx_task, .period = TASK_RATE / IR_UART_RX_TASK_RATE},
        {.func = ir_uart_tx_task, .period = TASK_RATE / IR_UART_TX_TASK_RATE},
        {.func = navswitch_task, .period = TASK_RATE / NAVSWITCH_TASK_RATE},
        {.func = button_task, .period = TASK_RATE / BUTTON_TASK_RATE},
        {.func = led1_task, .period = TASK_RATE / 10000},
    };     
    led_flash_task_init();
    button_init ();
    system_init ();
    ir_uart_init ();
    display_task_init();
    task_schedule (tasks, 6);
    
    return 0;
}
