#include "system.h"
#include "gamefunction.h"



/** Finds the winner for the current game by comparing the master and 
 * oponennet moves, changes the states for which player has one*/
void find_winner(uint8_t* master_char, uint8_t* opponent_char,
    Bool* mas_Won,Bool* opp_won){
     
     if(*master_char == ROCK && *opponent_char == SCISSORS ){
        *mas_Won = true;
        *opp_won = false;
     }
     else if(*master_char == SCISSORS && *opponent_char == ROCK  ){
         *mas_Won = false;
         *opp_won = true;
     }
     else if(*master_char == PAPER && *opponent_char == SCISSORS ){
        *mas_Won = false;
        *opp_won = true;
     }
    else if(*master_char == SCISSORS && *opponent_char == PAPER){
        *mas_Won = true;
        *opp_won = false;   
     }
     else if(*master_char == PAPER && *opponent_char == ROCK ){
        *mas_Won = true;
        *opp_won = false;
    }
     else if(*master_char == ROCK && *opponent_char == PAPER ){
         *mas_Won = false;
        *opp_won = true;
    } 
     //Assume draw
     else{
         *mas_Won = true;
         *opp_won = true;
    }
} 

 
/**  Displays the current winner by using mas_Won and sla_won as the parameters
 * once the winner is found the count is increased*/
char* display_winner(Bool* mas_Won, Bool* opp_won, uint8_t* master_win_count, uint8_t* opp_win_count){
    if  (*mas_Won == true && *opp_won == false){
        *master_win_count += 1;
        return WIN_DISPLAY;
    }
    else if(*mas_Won == false && *opp_won == true){
         *opp_win_count += 1;
         return LOOSE_DISPLAY;
    }
    else{
         return DRAW_DISPLAY;
    }
}


/**Displays a picture on the led matrix resembling a bit of paper uses tinygl*/
static void display_paper(void){
    tinygl_clear();
    //Pixel value is set to 1 for on and 0 for off
    tinygl_draw_box(tinygl_point (0, 0), tinygl_point (4, 6), 1); 
    tinygl_draw_line(tinygl_point (0, 2), tinygl_point (6, 2), 1);
    tinygl_draw_line(tinygl_point (0, 4), tinygl_point (6, 4), 1);
}


/**Displays a picture on the led matrix resembling a rock uses tinygl*/
static void display_rock(void){
    tinygl_clear();
    tinygl_text("*");
}


/**Displays a picture on the led matrix resembling some sicssors uses tinygl*/
static void display_sissors(void){
    //Pixel value is set to 1 for on and 0 for off
    tinygl_clear();
    tinygl_draw_line (tinygl_point (0, 6), tinygl_point (4, 6), 1);
    tinygl_draw_line (tinygl_point (0, 5), tinygl_point (4, 5), 1);
    tinygl_pixel_set (tinygl_point (2, 6), 0);
    tinygl_pixel_set (tinygl_point (2, 5), 0);
    tinygl_draw_point(tinygl_point (2, 4), 1);
    tinygl_draw_line (tinygl_point (0, 0), tinygl_point (2, 3), 1);
    tinygl_draw_line (tinygl_point (4, 0), tinygl_point (2, 3), 1);
}


/** Show the item at the parameter index and create tempory choice assignement 
 * at that index */ 
void show_char (uint8_t* index, uint8_t* choice){
    if (*index == 0){
        *choice = ROCK;
        display_rock();
    }
    else if (*index == 1){
        *choice = PAPER ;
        display_paper();
    }
    else{
        *choice = SCISSORS;
        display_sissors();
    }
}


 
 


 
 
