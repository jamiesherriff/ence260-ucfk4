#ifndef GAMEFUNCTION_H

#include "system.h"
#include "tinygl.h"

#define GAMEFUNCTION_H
#define WIN_DISPLAY "WINNER"
#define LOOSE_DISPLAY "LOOSER"
#define DRAW_DISPLAY "DRAW"
#define ROCK '*'
#define PAPER '#'
#define SCISSORS '>'
#define GAMENAME "ROCK PAPER SISSORS "
#define WAITMESSAGE "WaitingForOpponent "
#define OPPONENT_SCORE "OpponentScoreIs  "
#define PLAYER_SCORE "YourScoreIs  "
#define FINAL_WIN "CongratulationsYouWonTheGame!"
#define FINAL_LOOSE "GameOverYouLost"



typedef enum { false, true } Bool;

/* Calculates winner.  */
void find_winner(uint8_t* a, uint8_t* b, Bool* c,Bool* d );


/* Returns string win_display or loose_display and increases
 *  the score counter for the winner. */
char* display_winner(Bool* master_Won, Bool* slave_Won, uint8_t* master_win_count, 
                    uint8_t* opp_win_count);


/* Returns char rock_display if index = 0, paper_display if index = 1
 *  or scissors_display if index = 2 and sets the player choice to the above */
void show_char (uint8_t* index, uint8_t* choice);

#endif




